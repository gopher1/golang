package main

import "fmt"

func qn(n int) int {
	if n < 1 {
		return 1
	}
	return qn(n-1) * n
}

func sn(n int) int {
	if n < 1 {
		return 0
	}
	return sn(n-1) + qn(n)

}

func main() {
	fmt.Println(sn(3))
}
