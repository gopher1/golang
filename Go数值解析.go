package main

import (
	"fmt"
	"strconv"
)

func main() {
	a, b := 1, 2
	a, b = b, a
	fmt.Println(a, b)

	i, _ := strconv.Atoi("123")
	fmt.Println(i)
}
