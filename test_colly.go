package main

import (
	"fmt"

	"github.com/gocolly/colly"
)

func main() {
	// Instantiate default collector
	c := colly.NewCollector()

	// Visit only root url and urls which start with "e" or "h" on httpbin.org

	// On every a element which has href attribute call callback
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Text
		// Print link
		//fmt.Printf("Link found: %q -> %s\n", e.Text, link)
		// Visit link found on page
		fmt.Printf("Link found: %q -> %s\n", link)

		c.Visit(e.Request.AbsoluteURL(link))
	})

	// Before making a request print "Visiting ..."

	// Start scraping on https://hackerspaces.org
	c.Visit("http://video.kunming.cn/node_18560_20.htm")
}
