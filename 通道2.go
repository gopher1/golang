package main

import "fmt"

//有缓存channel 和 无缓存 channel 的区别. 有缓是异步的，无缓存是同步阻塞的
var ch = make(chan int)
var content string

func set() {
	content = "It's a unbuffered channel"
	println("111")
	ch <- 0

}

func main() {
	go set()

	<-ch

	fmt.Println(content)

}
