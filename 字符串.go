package main

import (
	"fmt"
	//"strings"
	"strconv"
)

/*
func main() {
	var str string = "This is an example of  a string "
	fmt.Printf("Dose the string %s have prefix %s ",str,"Th")
	fmt.Printf("%t\n", strings.HasPrefix(str,"Th"))
}
*/

/*
func main()  {
	var str string = "Hi ， I'm Marc,Hi."
	fmt.Printf("%d\n",strings.Index(str,"Marc"))
	fmt.Printf("%d\n",strings.Index(str,"Hi"))
	fmt.Printf("%d\n",strings.LastIndex(str,"Hi"))

}
*/

/*
//统计字符串出现次数
func main()  {
	var str string = "Hello,how is it going,Hugo?"
	var manyG = "gggggg"
	fmt.Printf("%d\n",strings.Count(str,"H"))
	fmt.Printf("%d\n",strings.Count(manyG,"gg"))
}
*/

/*
//重复字符串
func main()  {
	var old string = "Hi there!"
	var new string
	new = strings.Repeat(old,3)
	fmt.Printf("The new repeated string is %s\n",new)

}
*/

//修改字符串大小写

/*
func main()  {
	var old string = "Hey,how are you George? "
	var lower string
	var upper string
	fmt.Printf("%s\n",old)
	lower = strings.ToLower(old)
	fmt.Printf("%s\n",lower)
	upper = strings.ToUpper(old)
	fmt.Printf("%s\n",upper)

}
*/

/*
func main()  {
	str := "The quick brown fox jumps over the lazy dog"
	sl := strings.Fields(str)
	fmt.Printf("Splitted in slice:%v\n",sl)
	for _,val := range sl{
		fmt.Printf("%s -",val)
	}
	fmt.Println()
	str2 := "Go1| The ABC of Go | 25"
	sl2 := strings.Split(str2,"|")
	fmt.Printf("%v\n",sl2)
	for _,val :=range sl2{
		fmt.Printf("%s -",val)
	}
	fmt.Println()
	str3 := strings.Join(sl2,";")
	fmt.Printf("sl2 joined by:%s\n",str3)

}
*/

//字符串与其他类型的转换

func main() {

	var old string = "666"
	var an int
	var newS string
	fmt.Printf("The size of ints is %d\n", strconv.IntSize)

	an, _ = strconv.Atoi(old)
	fmt.Printf("The integer is %d\n", an)
	an += 5
	newS = strconv.Itoa(an)
	fmt.Printf("the new string is %s\n", newS)
}
