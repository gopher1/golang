package main

import (
	"context"
	"fmt"
)

func add1(ctx context.Context, a, b int) int {
	traceId := ctx.Value("trace_id").(string)
	fmt.Printf("trace_id:%v\n", traceId)
	return a + b
}
func calc(ctx context.Context, a, b int) int {
	traceId := ctx.Value("trace_id").(string)
	fmt.Printf("trace_id:%v\n", traceId)
	return add1(ctx, a, b)
}
func main() {
	ctx := context.WithValue(context.Background(), "trace_id", "123456")
	calc(ctx, 388, 200)
}
