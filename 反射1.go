package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	txt := `{"a":1,"b":2,"c":[{"name":"1","group":"2"},{"name":"3","group":"4"}]}`
	var m map[string]interface{}
	if err := json.Unmarshal([]byte(txt), &m); err != nil {
		panic(err)
	}
	/*
		v := reflect.ValueOf(m["c"])
			fmt.Println(v)
		count := v.Len()
		for i := 0; i < count; i++ {
			fmt.Println(v.Index(i))
		}
	*/

	res := m["c"].([]interface{})

	fmt.Println(res)
	for k, v := range res {
		fmt.Println(k, v)
	}
}
