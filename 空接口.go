package main

//import "fmt"

//空接口
//func main() {
//	var i interface{}
//	describe(i)
//
//	i = 42
//	describe(i)
//
//	i = "hello"
//	describe(i)
//}
//
//func describe(i interface{}) {
//	fmt.Printf("(%v, %T)\n", i, i)
//}

//空接口的作用， 类型判断

/*
func do(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Printf("Twice %v is %v\n", v, v*2)
	case string:
		fmt.Printf("%q is %v bytes long\n", v, len(v))
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}
}

func main() {
	do(21)
	do("hello")
	do(true)
}
*/
