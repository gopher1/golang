package main

import "fmt"

func fn(n int) int {
	if n < 2 {
		return 1
	}
	return fn(n-1) + fn(n-2)
}
func main() {

	for i := 0; i < 10; i++ {
		nums := fn(i)
		fmt.Println(nums)
	}
}
