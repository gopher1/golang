package main

import "fmt"

//指针方法和值方法都可以在指针或非指针上被调用,如下面程序所示,类型 List 在值上有一个方法 Len()
// 在指针上有一个方法 Append() ,但是可以看到两个方法都可以在两种类型的变量上被调用

type List []int

func (I List) Len() int {
	return len(I)
}

func (I *List) Append(val int) {
	*I = append(*I, val)

}

func main() {
	//值
	var lst List
	lst.Append(1)

	fmt.Printf("%v (len:%d)", lst, lst.Len())

	//指针
	plst := new(List)
	plst.Append(2)

	fmt.Printf("%v (len:%d)", plst, plst.Len())

}
