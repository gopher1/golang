package main

import "fmt"

type Rectangle struct {
	width, height float32
}

func area(r Rectangle) float32 {

	return r.height * r.width

}

func main() {
	r1 := Rectangle{2, 3}
	r2 := Rectangle{3, 4}

	fmt.Println(area(r1))
	fmt.Println(area(r2))
}
