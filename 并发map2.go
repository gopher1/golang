package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var m sync.Map

	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func(n int) {
			defer wg.Done()
			m.Store(fmt.Sprintf("%d", n), n)

		}(i)
	}

	wg.Wait()

	m.Range(func(key, value interface{}) bool {
		fmt.Println(key, value)
		return true
	})

}

//func main() {
//	var mu sync.Mutex
//	var wg sync.WaitGroup
//	q := []int{}
//
//	for i := 0; i < 100; i++ {
//		wg.Add(1)
//		go func(n int) {
//			defer wg.Done()
//			mu.Lock()
//			q = append(q, n)
//			mu.Unlock()
//
//		}(i)
//
//	}
//	wg.Wait()
//
//	fmt.Println(q)
//}
