package main

import "fmt"

/*
//闭包 引用了局部变量i
func main() {

	for i := 0; i < 3;i++{
		defer func(){
			fmt.Println(i)
		}()
	}
}
*/

func A1() {
	fmt.Println("Func A")
}

func B1() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Recover in B")

		}

	}()

	panic("Panic in B")
	println("panic 后的语句不会被执行")

}
func C1() {
	fmt.Println("Func C")
}

func main() {
	A1()
	B1()
	C1()

}

/*
func test(x,y int)  {
	z := 0
	func() {
		defer func() {
			if recover() != nil {
				z = 0
			}
		}()

		z = x / y
	}()

	println("x /y =",z)
}

func main()  {
	test(5,0)

}
*/
