package main

import (
	"fmt"
)

//空接口类似 Java/C# 中所有类的基类: Object 类,二者的目标也很相近。
// 可以给一个空接口类型的变量 var val interface {} 赋任何类型的值。

var i = 5
var str = "ABC"

type Person struct {
	name string
	age  int
}
type Any interface{}

func main() {
	var val Any

	val = 5
	fmt.Printf("val has the value %v\n", val)
	val = str
	fmt.Printf("val has the value: %v\n", val)
	pers1 := new(Person)
	pers1.name = "Rob Pike"
	pers1.age = 55
	val = pers1
	fmt.Printf("val has the value: %v\n", val)

	switch t := val.(type) {
	case int:
		fmt.Printf("Type int %T\n", t)
	case string:
		fmt.Printf("Type string %T\n", t)
	case *Person:
		fmt.Printf("Type pointer to Person %T\n", t)
	default:
		fmt.Printf("Unexpected type %T", t)

	}

}
