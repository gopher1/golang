package main

import (
	"fmt"
	"log"
	"regexp"

	"github.com/PuerkitoBio/goquery"
	"github.com/imroc/req"
)

func main() {

	r, err := req.Get("https://book.douban.com/top250")
	if err != nil {
		log.Fatal(err)
		return
	}

	item, err := goquery.NewDocumentFromResponse(r.Response())
	if err != nil {
		log.Fatal(err)
		return
	}

	item.Find(".item").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".pl2 > a").Text()

		reg := regexp.MustCompile(`[\r\n\t]+`)
		t := reg.ReplaceAllString(title, "")

		rate := s.Find(".rating_nums").Text()
		desc := s.Find("td > .pl").Text()
		quto := s.Find(".quote > span").Text()
		fmt.Println(t, rate, desc, quto)
	})

}
