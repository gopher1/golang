package main

import "fmt"

var slice []int

func tes(slice []int) {

	slice = append(slice, 5)
	fmt.Println(slice, len(slice), cap(slice))

}

func main() {
	slice = make([]int, 5, 10)
	tes(slice)
}
