package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func main() {
	data := []byte("testing")

	b := md5.Sum(data)
	//fmt.Println(string(b)) //错误，不能直接转 string
	fmt.Println(hex.EncodeToString(b[:]))
	fmt.Println(b[:])
}
