package main

import (
	"fmt"
	"strconv"
)

func main() {
	//字符串转数字
	i, _ := strconv.Atoi("-42")
	fmt.Println(i)
	//数字转字符串
	j := strconv.Itoa(42)
	fmt.Println(j)
}
