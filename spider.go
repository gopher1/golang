package main

import (
	"fmt"
	"github.com/imroc/req"

	"net/url"
	"time"

	"gopkg.in/go-playground/pool.v3"
	"gopkg.in/mgo.v2"
)

type T struct {
	Result []struct {
		SrcShortUrl string
		TitleUrl    string
		ImgUrl      string
		VideoUrl    string
		Duration    string
		Restitle    string
		Site_logo   string
		Nsclick_v   string
	}
}

func main() {

	t3 := time.Now()
	var p = pool.NewLimited(30)
	defer p.Close()
	keyword := url.QueryEscape("台风")

	batch := p.Batch()

	// for max speed Queue in another goroutine
	// but it is not required, just can't start reading results
	// until all items are Queued.

	go func() {
		for i := 0; i < 1000; i++ {
			str := fmt.Sprintf("http://app.video.baidu.com/app?word=%s&pn=%d&rn=50&order=1", keyword, i*50)
			batch.Queue(sendEmail(str))
		}

		// DO NOT FORGET THIS OR GOROUTINES WILL DEADLOCK
		// if calling Cancel() it calles QueueComplete() internally
		batch.QueueComplete()
	}()

	for email := range batch.Results() {

		if err := email.Error(); err != nil {
			// handle error
			// maybe call batch.Cancel()
		}

		// use return value
		fmt.Println(email.Value())
	}

	//pool := grpool.NewPool(30, 310)
	//defer pool.Release()
	//
	//pool.WaitCount(300)

	// submit one or more jobs to pool

	//pool.JobQueue <- func() {
	//	// say that job is done, so we can know how many jobs are finished
	//	defer pool.JobDone()
	//	 Get(str)

	delt := time.Since(t3)
	fmt.Println("爬虫结束,总共耗时: ", delt)
}

// wait until we call JobDone for all jobs
//pool.WaitAll()

func sendEmail(email string) pool.WorkFunc {

	return func(wu pool.WorkUnit) (interface{}, error) {

		Get(email)

		if wu.IsCancelled() {
			// return values not used
			return nil, nil
		}

		// ready for processing...

		return true, nil // everything ok, send nil, error if not
	}

}
func Get(url string) {
	session, err := mgo.Dial("")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	header := req.Header{
		"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"}
	req.SetTimeout(1 * time.Second)

	r, err := req.Get(url, header)

	//r, err := req.Get("http://www.yidianzixun.com/home/q/news_list_for_channel?channel_id=t5316&cstart={}&cend={}&infinite=true&refresh=1&__from__=pc&multi=5&appid=web_yidian&_=1498941230597.format(0,10)",header)
	if err != nil {

		fmt.Println(err)
		return

	}
	var t T

	r.ToJSON(&t)
	/*
		for _,v := range t.Result{
			c := session.DB("test").C("people")
			err = c.Insert(v)
			if err != nil {
				fmt.Println(err)
				return
			}
		}
	*/
	c := session.DB("test").C("people")
	err = c.Insert(t.Result[0])
	if err != nil {
		fmt.Println(err)
		return

	}

}

/*
func main() {
	t1 := time.Now()

	delt := time.Since(t1)
	fmt.Println("爬虫结束,总共耗时: ", delt)
}
*/
