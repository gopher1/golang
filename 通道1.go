package main

import "time"

//<- chan string 仅能接收字符串，而 chan <- string 仅能发送字符串

/*
func recevie(over chan <- bool) {

	over <- true
}
func main() {

	o := make( chan bool)
	go recevie(o)
	<- o
}
*/

func receive(c <-chan int, over chan<- bool) {
	for v := range c {
		println(v)
	}
	over <- true

}

func send(c chan<- int) {
	for i := 0; i < 3; i++ {
		c <- i
	}
	close(c)

}

func main() {

	c := make(chan int)
	o := make(chan bool)
	go send(c)
	go receive(c, o)
	time.Sleep(1 * time.Second)
	<-o

}
