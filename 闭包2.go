package main

import "fmt"

type decorate func()

func (this decorate) invoke(){
	//    println("before")
	this()
	//    println("after")
}

func myFunc() {
	println("blah")
}

func main() {
	x := decorate(myFunc)
	fmt.Println(x)
	x.invoke()
}