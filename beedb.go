package main

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

var (
	id   int
	age  int16
	name string
)

type User struct {
	//必须首字母大写
	Id   int
	Age  int16
	Name string
}


func init() {

	orm.RegisterDataBase("default", "mysql", "root:111111@tcp(127.0.0.1:3306)/gogo")
	orm.RegisterModel(new(User))
	// create table
	orm.RunSyncdb("default", false, true)

}

func insert() {
	orm.Debug = true
	o := orm.NewOrm()

	fmt.Println("Please enter your Name :")

	fmt.Scanln(&name)

	user := User{Name: name}

	fmt.Println(o.Insert(&user))
}

//高级查询
func query() {
	//Raw 凼数,迒回一个 RawSeter 用以对设置的 sql 诧句和参数迕行操作
	var j, i int64
	orm.Debug = true
	o := orm.NewOrm()
	var users []User
	num, err := o.Raw("SELECT age,id,name FROM user WHERE enabled=?", 1).QueryRows(&users)
	fmt.Println(users)
	if err == nil {
		fmt.Println("user nums", num)
	}
	i = num
	for j = 0; j < i; j++ {
		fmt.Printf("Element[%d]= %d\n", j, users[j].Name)
	}

}
func delete() {
	orm.Debug = true
	o := orm.NewOrm()

	user := User{Id: 9, Name: "koome"}
	fmt.Println(o.Delete(&user))

}

func update() {
	orm.Debug = true
	o := orm.NewOrm()

	user := User{Id: 8, Name: "koome_new"}
	fmt.Println(o.Update(&user))

}
func read() {
	o := orm.NewOrm()
	user := User{Id: 1}
	err := o.Read(&user)
	if err == orm.ErrNoRows {
		fmt.Println("查询不到")
	} else if err == orm.ErrMissPK {
		fmt.Println("找不到主键")

	} else {
		fmt.Println(user.Id, user.Name)
	}

}

func insertMulti() {

}

/*
func queryTable()  {
	o := orm.NewOrm()
	user1 = new(User)
	qs := o.QueryTable(user1)


}
*/

func main() {
	insert()

}
