package main

import (
	"fmt"
	"time"
)

func main() {

	start := time.Now()
	fmt.Println(start)
	time.Sleep(2 * time.Second)
	end := time.Now()
	fmt.Println(end)
	delta := end.Sub(start)

	println(delta)

	t1 := time.Now() // get current time
	time.Sleep(3 * time.Second)
	elapsed := time.Since(t1)
	fmt.Println("爬虫结束,总共耗时: ", elapsed)

}
