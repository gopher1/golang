package main

import "fmt"

// 绝对不要用指针指向 slice。切片本身已经是一个引用类型,所以它本身就是一个指针!!
//用 make() 创建一个切片

/*
func main() {
	var ar = [10]int{0,1,2,3,4,5,6,7,8,9}
	var a = ar[5:7]

	println(&a)
	for i := range a{
		println(a[i])
	}

}
*/

/*

func main()  {
	sl_from := []int{1,2,3}
	sl_to := make([]int,10)
	n := copy(sl_from,sl_to)
	println(sl_to)
	println(n)
}
*/

//字符串数组切片

/*
func main()  {
	s := "hello"
	c :=[]byte(s)
	c[0] = 'c'
	s2 := string(c)
	println(s2)

}
*/

//将切片传递给函数,计算素组元素和

/*
func sum(a []int)int  {
	s := 0
	for i := 0;i <len(a);i++{
		s += a[i]
	}

	return s
}

func main()  {
	 arr := [5]int{0,1,2,3,4}
	sum(arr[:])

}
*/

//用make 创建切片 slice := make([]type,len,cap)

//for range 结构
func main() {
	slice1 := make([]int, 10)
	for i := 0; i < len(slice1); i++ {
		slice1[i] = 5 * i
	}

	for i := 0; i < len(slice1); i++ {
		fmt.Printf("Slice at %d is %d\n", i, slice1[i])
	}
	for i, j := range slice1 {
		fmt.Printf("Slice at %d is %d\n", i, j)
	}

}
