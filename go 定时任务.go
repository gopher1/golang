package main

import "time"
import "log"

func main() {

	timer := time.NewTicker(2 * time.Second)
	for {
		select {
		case <-timer.C:
			go func() {
				log.Println(time.Now())
			}()
		}
	}
}
