package main

import (
	"encoding/json"
	"fmt"

	"sync"
)

type Foo struct {
	sync.Map
}

func (f *Foo) UnmarshalJSON(data []byte) error {
	var tmpMap map[string]interface{}
	if err := json.Unmarshal(data, &tmpMap); err != nil {
		return err
	}
	for key, v := range tmpMap {
		fmt.Println(key, v)
		f.Store(key, v)
	}
	return nil
}

func main() {
	x := &Foo{}
	date := []byte(`{"127.1":{"host":"host1","list":["list123","list456"]},"127.2":{"host":"host2","list":["list223","list256"]}}`)
	x.UnmarshalJSON(date)
}
