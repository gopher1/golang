package main

import "fmt"

type Per string

//使用defer 语句实现代码追踪
func trace(s string) {
	println("enter", s)

}

func untrace(s string) {
	println("leaving", s)

}

func a() {
	trace("a")
	defer untrace("a")
	println("in a")

}

func b() {
	trace("b")
	defer untrace("b")
	println("in b")
	a()

}

func (r Per) sum(x, y int) (c int) {
	c = x / y
	fmt.Println(r, c)
	return

}

//函数接受参数。在 Go; 中，函数可以返回多个“结果参数”，而不仅仅是一个值。它们可以像变量那样命名和使用。

//如果命名了返回值参数，一个没有参数的 return; 语句，会将当前的值作为返回值返回
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

type fun func(int)

func myfn1(i int) {
	fmt.Printf("\ni is %v", i)
}
func myfn2(i int) {
	fmt.Printf("\ni is %v", i)
}
func test(f fun, val int) {
	f(val)
}

type Count int

func (c *Count) Incr() int {
	*c = *c + 1
	return int(*c)
}

func main() {
	var c Count = 1
	fmt.Println(c.Incr())

}
