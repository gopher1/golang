package main

/*
//将函数作为参数
func Add(a,b int) int {
	return a+b

}

func callback(y int , f func(int,int)int)  (z int) {
	z = f(y,2)
	return z

}




func main() {

	q := callback(1,Add)
	println(q)


}
*/

//简洁版函数交换
//func swa(x,y,z int)(int,int,int)  {
//
//	return z,y,x
//
//}

func main() {
	//元组中元素的位置交换
	//a,b,c := swa(1,2,3)
	//println(a,b,c)
	a := 1

	b := 2
	//c := 3
	//a,b,c = c,b,a
	//
	//println(a,b,c)
	swap1(a, b)

}
func swap1(x, y int) int {
	var temp int

	temp = x /* 保存 x 的值 */
	x = y    /* 将 y 值赋给 x */
	y = temp /* 将 temp 值赋给 y*/

	return temp
}
