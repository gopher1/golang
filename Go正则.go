package main

import (
	"bytes"
	"fmt"
	"regexp"
)

func main() {
	match, _ := regexp.MatchString("p[a-z]+ch", "peach")
	fmt.Println("1", match)

	r, _ := regexp.Compile("p[a-z]+ch")

	fmt.Println("2", r.MatchString("peach"))

	// 3. 这是查找匹配字符串的。
	fmt.Println("3", r.FindString("peach punch"))

	// 4. 这个也是查找第一次匹配的字符串的，但是返回的匹配开始和结束位置索引，而不是匹配的内容。
	fmt.Println("4", r.FindStringIndex("peach"))

	// 5. Submatch 返回 完全匹配 和 局部匹配 的字符串。例如，这里会返回 p([a-z]+)ch 和 ([a-z]+) 的信息。
	fmt.Println("5.", r.FindStringSubmatch("peach punch"))

	// 6. 类似的，这个会返回 完全匹配 和 局部匹配 的索引位置。
	fmt.Println("6.", r.FindStringSubmatchIndex("peach punch"))

	// 7. 带 All 的这个函数返回所有的匹配项，而不仅仅是首次匹配项。例如查找匹配表达式的所有项。
	fmt.Println("7.", r.FindAllString("peach punch pinch", -1))

	// 8. All 同样可以对应到上面的所有函数。
	fmt.Println("8.", r.FindAllStringSubmatchIndex("peach punch pinch", -1))

	// 9. 这个函数提供一个正整数来限制匹配次数。
	fmt.Println("9.", r.FindAllString("peach punch pinch", 2))

	// 10. 上面的例子中，我们使用了字符串作为参数，并使用了如 MatchString 这样的方法。我们也可以提供 []byte参数并将 String 从函数命中去掉。
	fmt.Println("10.", r.Match([]byte("peach")))

	// 11. 创建正则表示式常量时，可以使用 Compile 的变体MustCompile 。因为 Compile 返回两个值，不能用语常量。
	r = regexp.MustCompile("p([a-z]+)ch")
	fmt.Println("11.", r)

	// 12. regexp 包也可以用来替换部分字符串为其他值。
	fmt.Println("12.", r.ReplaceAllString("a peach", "<fruit>"))

	// 13. Func 变量允许传递匹配内容到一个给定的函数中，
	in := []byte("a peach")
	out := r.ReplaceAllFunc(in, bytes.ToUpper)
	fmt.Println("13.", string(out))
}
