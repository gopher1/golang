package main

import (
	"fmt"
	"time"
)

//结构体类型的方法
/*
type TwoInts struct {
	a int
	b int
}

func (tn TwoInts) addThem() int  {
	return tn.a + tn.b

}

func (tn TwoInts) AddToParam(param int) int {
	return tn.a + tn.b + param

}


func main() {
	two1 := new(TwoInts)
	two1.a = 12
	two1.b = 10

	fmt.Printf("%d\n" ,two1.addThem())
	fmt.Printf("%d\n",two1.AddToParam(20))
	two2 := TwoInts{3,4}
	fmt.Printf("%d\n",two2.addThem())
}
*/

//非结构体类型的方法

/*
type IntVector []int

func (v IntVector) Sum() (s int)  {
	for _,x := range v{
		s += x
	}
	return
}

func main()  {
	fmt.Println(IntVector{1,2,3}.Sum())

}
*/

type myTime struct {
	time.Time
}

func (t myTime) first3Chars() string {
	return t.Time.String()[0:3]

}

func main() {
	m := myTime{time.Now()}
	fmt.Println("full time now:", m.String())
	fmt.Println("First 3 chars:", m.first3Chars())

}
