package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y float64
}

type NamedPoint struct {
	Point
	name string
}

func (p *Point) Abs() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)

}

//可以覆写方法(像字段一样):和内嵌类型方法具有同样名字的外层类型的方法会覆写内嵌类型对应的方法。

func (n *NamedPoint) Abs() float64 {
	return n.Point.Abs() * 100

}

func main() {
	//方法的继承
	n := &NamedPoint{Point{3, 4}, "Pythagoras"}
	fmt.Println(n.Abs())
}
