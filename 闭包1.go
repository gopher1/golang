package main

import "fmt"

func decorator(f func(s string)) func(s string)  {
	return func(s string) {
		fmt.Println("ha")
		f(s)
		fmt.Println("wo")
	}

}

func Hello(s string)  {
	fmt.Println(s)
}

func main()  {
	decorator(Hello)("你好")
}