package main

import (
	"fmt"
	"time"
)

/*
var ch = make(chan int)
var content string

func set()  {
	content = "It's a unbuffered channel"

	ch <- 0

}

func main()  {
	go set()
	<- ch

	fmt.Println(content)


}
*/

/*
func main()  {
	tick := make(chan int,1)

	go func(){
		time.Sleep(2*time.Second)
		count := 1
		for{
			time.Sleep(1*time.Second)
			tick <- count
			count++
		}
	}()
	//可以用迭代器从通道中取数据，tick 是非缓冲通道，所以只有当通道有数据时，这里才会继续执行
	for v := range tick{
		fmt.Printf("%d\n",v)
		if v == 5{
			break
		}
	}

}
*/

func main() {
	tick := make(chan int, 1)
	go func() {
		time.Sleep(2 * time.Second)
		count := 1
		for {
			time.Sleep(1 * time.Second)
			tick <- count
			if count == 5 {
				close(tick)
			}
			count++

		}

	}()
	// 当通道被关闭时迭代器执行会自动结束
	for v := range tick {
		fmt.Printf("%d\n", v)
	}

}

/*
func main()  {
	c := make(chan bool)
	go func(){
		fmt.Println("Go,Go,Go!!!")
		c <- true
		close(c)





	}()
	for v := range c{
		fmt.Println(v)

	}



}

*/
