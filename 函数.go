package main


func add(x, y int) int {
	return x + y
}

func swap(x, y string) (string, string) {
	return y, x
}

func max(num1, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

func main() {


	var ret int
	ret = add(3, 4)
	//函数多返回值
	ret1, ret2 := swap("张三", "李四")
	print(ret, ret1, ret2)

	ret3 := max(100, 200)
	println(ret3)
	//引用的理解
	a := 1
	b := 2
	println(&a, &b)
	var temp int

	temp = a

	a = b
	println(&a)
	b = temp
	println(a, b)
	println(&a, &b)

}
