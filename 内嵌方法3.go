package main

import "fmt"

type Camera struct{}

func (self Camera) Takeicture() string {
	return "Click"

}

type Phone struct{}

func (self Phone) Call() string {
	return "Rang Rang"

}

type CameraPhone struct {
	Camera
	Phone
}

func main() {
	self := new(CameraPhone)
	fmt.Println(self.Takeicture(), self.Call())
}
