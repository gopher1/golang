package main

import "fmt"

func main() {

	ch := make(chan int) //改为 ch := make(chan int, 1) 就好了

	go func() {
		ch <- 10
	}()
	<-ch
	fmt.Println("success")

}
