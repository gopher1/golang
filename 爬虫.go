package main

import (
	"fmt"
	"github.com/imroc/req"
	"gopkg.in/mgo.v2"
	"log"
	//"github.com/saintfish/chardet"
	//"github.com/isdamir/mahonia"
	//"encoding/json"
)

type M struct {
	Result []struct {
		SrcShortUrl string
		TitleUrl    string
		ImgUrl      string
		VideoUrl    string
		Duration    string
		Restitle    string
		Site_logo   string
		Nsclick_v   string
	}
}

//func goqueryDemo() {
//	doc, err := goquery.NewDocument("http://www.baidu.com")
//	if err != nil {
//		fmt.Println(err)
//	}
//	fmt.Println(doc)
//	head := doc.Find("head")
//	meta := head.Find("meta")
//	content, _ := meta.Attr("content")
//	fmt.Println("content is:", content)
//}
func main() {

	//var gr gResult
	header := req.Header{
		"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"}
	r, err := req.Get("http://app.video.baidu.com/app?word=%E5%8F%B0%E5%8C%97&pn=0&rn=4&order=1", header)
	//r, err := req.Get("http://www.yidianzixun.com/home/q/news_list_for_channel?channel_id=t5316&cstart={}&cend={}&infinite=true&refresh=1&__from__=pc&multi=5&appid=web_yidian&_=1498941230597.format(0,10)",header)
	if err != nil {
		log.Fatal(err)

	}

	var m M

	r.ToJSON(&m)
	fmt.Println(m.Result[0])
	//if err := json.Unmarshal([]byte(r.String()), &m); err != nil {
	//	panic(err)
	//}

	//for  _,v := range m.Result {
	//	fmt.Printf("%+v\n",v)
	//}
	session, err := mgo.Dial("")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	c := session.DB("test").C("people")
	err = c.Insert(m.Result[0])
	if err != nil {
		fmt.Println(err)
		return
	}

	/*
	   	detector := chardet.NewTextDetector()
	   	result, err := detector.DetectBest(r.Bytes())
	   	if err == nil {
	   		fmt.Printf(
	   			"Detected charset is %s, language is %s",
	   			result.Charset,
	   			result.Language)
	   		fmt.Println()
	   		enc := mahonia.NewDecoder(result.Charset)
	   		fmt.Println(enc.ConvertString(r.String()))
	   	}


	       var m map[string]interface{}

	   	fmt.Println(r.ToJSON(&m))

	   	result := m["result"].([]interface{})

	   	for _,v := range result{
	   		fmt.Println(v)
	   	}

	*/

}
