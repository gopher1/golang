// 根据哈工大的基于行块分布函数的通用网页正文抽取算法改进对通用HTML进行正文提取
package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/isdamir/mahonia"
)

// Article 定义文章提取内容
type Article struct {
	Title   string
	Content string
}

func main() {
	url := os.Args[1]
	fmt.Printf("获取网页:%s\n", url)
	html := getHTML(url)
	if html == nil {
		fmt.Println("获取源码失败")
		return
	}
	encoding := getEncoding(html)
	if encoding == "" {
		return
	}
	encoding = strings.ToLower(encoding)
	if encoding == "gb2312" {
		encoding = "gbk"
	}
	source := convertToUtf8(string(html), encoding)
	if len(source) > 1 {
		// fmt.Println(source)
		fmt.Println("网页编码:", encoding)
		article := Extractor(source)
		fmt.Println("网页标题:", article.Title)
		fmt.Println("网页正文:", article.Content)
	}

}

// 将其它编码的字符串转换为utf-8编码
func convertToUtf8(src, encoding string) string {
	enc := mahonia.NewDecoder(encoding)
	return enc.ConvertString(src)
}

// 获取网页编码
func getEncoding(html []byte) string {
	var ret string
	re, _ := regexp.Compile(`(?is)<meta.*?charset=[\"']?([\w-]+?)[\"'].*?>`)
	defer func() {
		if p := recover(); p != nil {
			fmt.Println(re.FindSubmatch(html))
			ret = ""
		}
	}()
	encoding := re.FindSubmatch(html)[1]
	// ret = *(*string)(unsafe.Pointer(&encoding))
	ret = string(encoding)
	return ret
}

// 获取网页源码
func getHTML(url string) []byte {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("User-Agent", "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html）")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer resp.Body.Close()
	if resp.StatusCode == 200 {
		var body []byte
		switch resp.Header.Get("Content-Encoding") {
		case "gzip":
			var collections [][]byte
			reader, _ := gzip.NewReader(resp.Body)

			for {
				buf := make([]byte, 1024)
				n, err := reader.Read(buf)
				if err != nil && err != io.EOF {
					return nil
				}
				if n == 0 {
					break
				}
				collections = append(collections, buf)
			}
			body = bytes.Join(collections, nil)
		default:
			body, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Println("can't get code")
				return nil
			}
		}
		return body
	}
	fmt.Println(resp.StatusCode)
	return nil
}

// 获取网页标题title标签部分
func getTitle(html string) (string, error) {
	re, err := regexp.Compile(`(?is)<title.*?>([^<]+?)</title>`)
	if err != nil {
		return "", fmt.Errorf("can't get title: %s", err)
	}
	titleRes := re.FindStringSubmatch(html)
	title := titleRes[1]
	defer func() {
		if p := recover(); p != nil {
			title = ""
		}
	}()
	// 有的title有换行符的需要把换行符干掉
	reg, _ := regexp.Compile(`[\r\n\t]+`)
	title = reg.ReplaceAllString(title, "")
	return title, nil
}

// 获取网页body源码
func getBody(html string) (string, error) {
	re, _ := regexp.Compile("(?is:<body.*?>.+?</body>)")
	return re.FindString(html), nil
}

// 去除body中的style, script, noscript以及网页的注释等无效元素
func dropInvalidTag(html string) string {
	style, _ := regexp.Compile("(?is:<style.*?>.*?</style>)")
	script, _ := regexp.Compile("(?is:<script.*?>.*?</script>)")
	noscript, _ := regexp.Compile("(?is:<noscript.*?>.*?</noscript>)")
	comment, _ := regexp.Compile("(?is:<!--.*?-->)")
	text := style.ReplaceAllString(html, "")
	text = script.ReplaceAllString(text, "")
	text = noscript.ReplaceAllString(text, "")
	return comment.ReplaceAllString(text, "")
}

// 格式化HTML标签, 主要针对的是源码进行压缩后的网页
// 对连续的a标签, li标签进行处理, 避免链接块对结果的影响
// 对p标签换行处理以及行内属性换行造成一个元素分布在多行的处理
// 去除\r避免对后期输出造成影响
func prettyCode(html string) string {
	lineCount := strings.Count(html, "\n")
	if lineCount < 5 {
		re, _ := regexp.Compile("><")
		html = re.ReplaceAllString(html, ">\n<")
	}
	aTag, _ := regexp.Compile(`(?i:</a>\s*?<a)`)
	liTag, _ := regexp.Compile(`(?i:</li>\s*?<li)`)
	divTag, _ := regexp.Compile(`(?i:</div>)`)
	pTag, _ := regexp.Compile(`(?i)(<p[^<\n]*?>)\s*?[\r\n]*?([^<\n]*?)[\r\n]*?(</p>)`)
	dropSlashR, _ := regexp.Compile(`\r`)
	dropBlank, _ := regexp.Compile(`(?i)(<\w+[^>]*?)\n+(.*?>)`)
	html = aTag.ReplaceAllString(html, "</a>\n<a")
	html = liTag.ReplaceAllString(html, "</li>\n<li")
	html = divTag.ReplaceAllString(html, "</div>\n")
	html = pTag.ReplaceAllString(html, "$1$2$3")
	html = dropSlashR.ReplaceAllString(html, "")
	for dropBlank.MatchString(html) {
		html = dropBlank.ReplaceAllString(html, "$1$2")
	}
	return html
}

// 去除所有标签
// 去除行中的所有空白字符
// 为避免干扰所有的a标签将替换为2个字符
func dropTags(html string) string {
	dropATag, _ := regexp.Compile("(?is:<a[^>]*?>.*?</a>)")
	dropOtherTag, _ := regexp.Compile(`(?is:<[/\w]+.*?>)`)
	dropBlank, _ := regexp.Compile(`(?is:[ \r\t]+)`)
	html = dropATag.ReplaceAllString(html, "aa")
	html = dropOtherTag.ReplaceAllString(html, "")
	return dropBlank.ReplaceAllString(html, "")
}

// 计算正文位置并提取出正文
// body 为body部分源码
// k 为行块厚度
// startCount 为正文开始行块文本最小长度
// endCount 为正文结束行块文本最小长度
// currentLen 为当前行长度
// blockLen 为当前行块长度
// rawLines 为原始body字符串切片
// cleanLines 为去除标签以及空白字符后的body字符串切片
// startPos 为正文开始位置
// endPos 为正文结束位置
func calculateArticle(body string) string {
	k := 5
	startCount := 150
	endCount := 20
	startPos := -1
	endPos := 0
	re, _ := regexp.Compile(`\n`)
	rawLines := re.Split(body, -1)
	cleanLines := []string{}
	totalLen := 0
	for _, line := range rawLines {
		cline := dropTags(line)
		cleanLines = append(cleanLines, cline)
		totalLen++
	}
	for ix, line := range cleanLines[:totalLen-k] {
		currentLen := len([]rune(line))
		blockLen := 0
		for _, item := range cleanLines[ix : ix+k] {
			blockLen += len([]rune(item))
		}
		if startPos == -1 && blockLen > startCount && currentLen > 0 {
			emptyLine := 0
			for i := 1; i <= k; i++ {
				if emptyLine >= 2 {
					startPos = ix - i + 2
					break
				}
				if len(cleanLines[ix-i]) == 0 {
					emptyLine++
				} else {
					emptyLine = 0
				}
			}
			if startPos == -1 {
				startPos = ix
			}
		}
		if startPos != -1 && blockLen < endCount && currentLen == 0 {
			endPos = ix
			break
		}
	}
	if endPos == 0 {
		endPos = totalLen - k
	}
	if startPos == -1 {
		return ""
	}
	result := rawLines[startPos:endPos]
	return strings.Join(result, "")
}

// Extractor 网页正文提取函数
func Extractor(html string) Article {
	article := Article{}
	title, err := getTitle(html)
	body, err := getBody(html)
	if err != nil {
		return article
	}
	cleanHTML := dropInvalidTag(body)
	prettyHTML := prettyCode(cleanHTML)
	rawContent := calculateArticle(prettyHTML)
	article.Title = title
	article.Content = rawContent
	return article

}

// 格式化输出正文
// func outPutFormat(html string) string {

// }
