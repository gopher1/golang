package main

import (
	"bufio"
	"fmt"
	"os"
)

// 从键盘和标准输入 os.Stdin 读取输入，用 Scan 和 Sscan 函数
/*
var(
	firstName,lastName, s      string
	ii int
	xx float32
	input = "5612 /52.12 /Go"
	format = "%d /%f  %s"
)

func main() {
	fmt.Println("Please enter your name a,b :")
	fmt.Scanln(&firstName,&lastName)
	fmt.Printf("HI %s %s! \n",firstName,lastName)
	fmt.Sscanf(input,format,&ii,&xx,&s)
	fmt.Println("From the string we read:",ii,xx,s)

}
*/
//用 bufio 包提供的缓冲读取(buffered reader)来读取数据

/*
var inputReader *bufio.Reader
var input string
var err error

func main()  {
	inputReader = bufio.NewReader(os.Stdin)
	fmt.Println("Please enter some input: ")
	input,err = inputReader.ReadString('\n')
	if err == nil{
		fmt.Printf("The input was %s\n",input)
	}

}
*/

/*
func main()  {
	fmt.Println("Please enter some input: ")
	inputReader := bufio.NewReader(os.Stdin)
	input,err := inputReader.ReadString('\n')
	if err == nil{
		fmt.Printf("The input was %s\n",input)
	}
}
*/

func main() {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter your name:")
	input, err := inputReader.ReadString('\n')

	if err != nil {
		fmt.Println("There were errors reading, exiting program.")
		return
	}
	fmt.Printf("Your name is %s", input)
	//switch input{
	//case "Philip\n" : fmt.Println("hello Philip")
	//case "Chris\n" : fmt.Println("hello Chris")
	//case "Ivo \n" : fmt.Println("hello Ivo")
	//default: fmt.Printf("You are not welcome here! Goodbye!")
	//
	//
	//
	//}
	// version 2:
	switch input {
	case "Philip\n":
		fallthrough
	case "Ivo\n":
		fallthrough
	case "Chris\n":
		fmt.Printf("Welcome %s\n", input)
	default:
		fmt.Printf("You are not welcome here! Goodbye!\n")
	}

}
