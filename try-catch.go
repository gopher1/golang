package main

import (
	"fmt"

	"github.com/manucorporat/try"
)

func main() {

	try.This(func() {

		var c int = 1 / 0
		fmt.Println(c)
	}).Finally(func() {
		fmt.Println("end")
	}).Catch(func(e try.E) {
		// Print crash
		fmt.Println(e)
	})
}
