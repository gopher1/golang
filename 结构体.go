package main

import (
	"fmt"
)

// var t *T
// t = new(T)

/*
type struct1 struct {
	i1 int
	f1 float32
	str string
}

func main() {

	ms := new(struct1)
	ms.i1 = 10
	ms.f1 = 15.5
	ms.str = "Chris"
	fmt.Printf("%d\n",ms.i1)
	fmt.Printf("%f\n",ms.f1)
	fmt.Printf("%s\n",ms.str)
	fmt.Println(ms)


}
*/

/*
type Person struct {
	firstName string
	lastName string

}

func upPerson(p *Person)  {
	p.firstName = strings.ToUpper(p.firstName)
	p.lastName = strings.ToUpper(p.lastName)

}

func main()  {
	var pers1 Person
	pers1.firstName = "chris"
	pers1.lastName = "Woodward"
	upPerson(&pers1)
	fmt.Println(pers1.firstName,pers1.lastName)

}
*/

type aa struct {
	Name string
}

func main() {
	m := aa{"qq"}
	n := new(aa)
	n.Name = "haha"
	fmt.Println(m)
	fmt.Println(n)
}
