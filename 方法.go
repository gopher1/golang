package main

import "fmt"

//指针与 值传递的区别

type AA struct {
	Name string
}

type BB struct {
	Name string
}

func (a *AA) Print() {
	a.Name = "AAA"

}

func (b *BB) Print() {
	b.Name = "BBB"

}

func main() {
	a := &AA{"qq"}
	a.Print()
	fmt.Print(a.Name)
	b := &BB{"yy"}
	b.Print()
	fmt.Println(b.Name)
}
