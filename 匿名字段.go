package main

import "fmt"

type Skills []string

type Human struct {
	name   string
	age    int
	weight int
}

type Student struct {
	Human
	Skills
	int        //内置类型作匿名字段
	speciality string
}

func main() {

	jane := Student{Human{"xx", 10, 10}, []string{"haha"}, 10, "a"}
	fmt.Println(jane)

}
