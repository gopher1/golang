package main

import "fmt"

/*
func main() {
/*	a := 100;
	if a < 20{
		println("a小于20")
	}else {
		println("a大于20")
	}
*/
//	go语言if语句嵌套
/*
	a := 100
	b := 200
	if a == 100{
		if b == 200{
			println(a,b)
		}

	}

//	for循环
	b := 15
	var a int
	//go 语言数组
	num := [6]int{1,2,3,5}

	fmt.Println(a,b,num)


}
*/

/*
func main()  {

	numbers := [5]int{1, 2, 3, 5}
	for i,x := range numbers{
		fmt.Println(i,x)
	}

}
*/

/*
func main()  {
	//指针数组和数组指针
	a := [...]int{1,2}
	p := &a

	fmt.Println(p[1],&a[0],&a[1])

}
*/

/*
func main()  {

	str := "Go is a beautiful language!"
	fmt.Println("%d\n",len(str))
	for i := 0; i < len(str);i++{
		fmt.Printf("Character on position %d is %c\n",i,str[i])
	}

}
*/
/*
//for range

func main()  {

	str := "Go is a beautiful language!"
	fmt.Println("%d\n",len(str))
	for pos, char := range str{
		fmt.Printf("Character on position %d is %c\n",pos,char)
	}
	fmt.Println()
	str2 := "Chinese: 日本語"
	fmt.Printf("The length of str2 is: %d\n", len(str2))
	for pos, char := range str2 {
		fmt.Printf("character %c starts at byte position %d\n", char, pos) }

}
*/
func main() {
	s := ""
	for s != "aaaaa" {
		fmt.Println("Value of s:", s)
		s = s + "a"
	}
}
