package main

import (
	"fmt"
	"sort"
)

/*
func main() {
	var value int
	var isPresent bool
	map1 := make(map[string]int)
	map1["New Delhi"] = 55
	map1["Beijing"] = 20
	map1["Washington"] = 25
	value,isPresent = map1["Beijing"]
	if isPresent{
		fmt.Println("The value of Beijing in map1 is %d\n",value)
	}else {
		fmt.Println("map1 does not contain Beijing")
	}
	value,isPresent = map1["paris"]
	fmt.Printf("%t\n",isPresent)
	fmt.Println("value is %d\n",value)
	delete(map1,"Washington")
	value,isPresent = map1["Washington"]
	if isPresent{
		fmt.Println("The value of Washington in map1 is %d\n",value)
	}else {
		fmt.Println("map1 does not contain Washington")
	}
}
*/

/*
func main()  {
	map1 := make(map[int]float32)
	map1[1] = 1.0
	map1[2] = 1.0
	map1[3] = 1.0
	map1[4] = 1.0
	for key,value := range map1{
		fmt.Printf("Key is %d - value is %f\n",key,value)
	}
}
*/

//map 容量 make(map[keytype]valuetype, cap)
//map 类型的切片

/*
func main()  {

	items := make([]map[int]int,5)
	for i := range items{
		items[i] = make(map[int]int,1)
		items[i][1] = 2

	}
	fmt.Printf("value of items:%v\n",items)
}
*/

//if _,ok := map1[key1];ok{}

/*
//map 切片
func main()  {
	slice := make([]map[int]string,5)
	for i := range slice{
		slice[i] = make(map[int]string)
		slice[i][i] = "OK"
		fmt.Println(slice[i])
	}

}
*/

/*
//map 排序

func main()  {
	m := map[int]string{1:"a",2:"b",3:"c",4:"d"}
	s := make([]int, len(m))
	i := 0
	for k,_ := range m{
		s[i] = k
		i++
	}
	sort.Ints(s)
	fmt.Println(s)
	for _,k := range s{
		fmt.Printf("Key:%v,Value:%v\n",k,m[k])
	}

}

var (
	barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
		"delta": 87, "echo": 56, "foxtrot": 12,
		"golf": 34, "hotel": 16, "indio": 87,
		"juliet": 65, "kili": 43, "lima": 98}
)

func main() {
	fmt.Println("unsorted:")
	for k, v := range barVal {
		fmt.Printf("Key: %v, Value: %v / ", k, v)
	}
	keys := make([]string, len(barVal))
	i := 0
	for k, _ := range barVal {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	fmt.Println()
	fmt.Println("sorted:")
	for _, k := range keys {
		fmt.Printf("Key: %v, Value: %v / ", k, barVal[k])
	}
}
*/

//map 键值交换

var (
	barVal = map[string]int{"alpha": 34, "bravo": 56, "charlie": 23,
		"delta": 87, "echo": 56, "foxtrot": 12,
		"golf": 34, "hotel": 16, "indio": 87,
		"juliet": 65, "kili": 43, "lima": 98}
)

func main() {
	invMap := make(map[int]string, len(barVal))
	for k, v := range barVal {
		invMap[v] = k
	}
	fmt.Println("inverted:")
	for k, v := range invMap {
		fmt.Printf("Key: %v, Value: %v / ", k, v)
	}
}
