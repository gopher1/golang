package main

import "fmt"

//go 语言中的闭包

/*
func closure(x int) func(y  int) int  {
	return func(y int) int {
		return x + y
	}

}

func main() {

	f := closure(10)
	f(1)
	println(f(1))
}

*/

/*
func MinMax(a,b int)(min,max int)  {
	if a < b{
		min = a
		max = b
	}else {
		min = b
		max = a
	}
	return
}


func main()  {

	min,max := MinMax(20,16)
	println(min,max)
}

*/

/*
//指针的理解相当于Python中的引用，装饰器，list
func A(a *int)  {
	*a = 2
	fmt.Println(*a)
}

func main()  {
	a := 4
	fmt.Println(a)
	A(&a)
	fmt.Println("xxx")
	fmt.Println("这是第二个",a)
}
*/

/*
func f()  {
	for i := 0;i < 4;i++{
		g := func(i int) {fmt.Printf("%d",i)}
		g(i)
		fmt.Printf(" -q  is of type %T and has %v\n",g,&g)
	}
}

func main()  {
	f()

}
*/

func fib() func() int {
	a, b := 0, 1

	return func() int {

		a, b = b, a+b
		return a
	}
}

func main() {
	q := fib()

	for i := 0; i < 10; i++ {
		fmt.Println(q())
	}
}
