package main

type Engine interface {
	Start()
	Stop()
}
type Car struct {
	Engine
}

func (c *Car) GoToWorkIn() {
	c.Stop()
	c.Start()

}

func main() {

	c1 := new(Car)
	c1.GoToWorkIn()

}
