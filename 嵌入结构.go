package main

import "fmt"

//匿名字段和内嵌结构
/*
type human struct {
	sex int
}
type teacher struct {
	human
	Name string
	Age int
}
type student struct {
	human
	Name string
	Age int
}
func main() {

	a := teacher{Name:"joe",Age: 19,human:human{sex : 1}}
	a.Name = "joe2"
	a.Age = 13
	a.sex = 3
	fmt.Println(a)
}
*/

type A struct {
	B
	Name string
}
type B struct {
	Name string
}

func main() {
	a := A{Name: "A", B: B{Name: "B"}}
	fmt.Println(a.Name, a.B.Name)
}
