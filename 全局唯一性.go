package main

import (
	"sync"
	"fmt"
)

var once sync.Once
var qq string
var ccc = make(chan int, 2)

func setup() {
	qq = "Hello World"
	fmt.Println(qq)

}
func drop() {
	//保证setup 全局只执行一次
	once.Do(setup)

	ccc <- 1

}
func two() {
	for i := 0; i < 2; i++ {
		go drop()
	}
	for i := 0; i < 2; i++ {
		<-ccc
	}

}

func main() {

	two()
}
