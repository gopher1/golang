package main

import (
	"fmt"
	"regexp"

	"github.com/gocolly/colly"
)

func main() {

	c := colly.NewCollector()
	d := colly.NewCollector()
	// On every a element which has href attribute call callback
	c.OnHTML(".item .pl2 > a", func(e *colly.HTMLElement) {
		title := e.Text
		href := e.Request.AbsoluteURL(e.Attr("href"))
		//match, _ := regexp.MatchString("\r\n\t", link)
		// Print link
		reg, _ := regexp.Compile(`[\r\n\t]+`)
		title = reg.ReplaceAllString(title, "")

		d.OnRequest(func(r *colly.Request) {
			r.Ctx.Put("title", title)
		})
		d.Visit(href)

	})
	d.OnResponse(func(r *colly.Response) {
		fmt.Println(r.Ctx.Get("title"))
	})
	d.OnHTML("#info", func(e *colly.HTMLElement) {
		text := e.Text

		fmt.Println(text)

	})

	c.Visit("https://book.douban.com/top250")
}
